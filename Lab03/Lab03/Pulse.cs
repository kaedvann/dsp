﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab01
{
    public class Pulse
    {
        protected const int N = 1000;
        private readonly Func<double, double> _pulseFunc;
        private readonly double _period;

        private static double Sinc(double x )
        {
            return Math.Sin(x)/x;
        }

        public static Pulse CreateGaussPulse(double amplitude, double sigma)
        {
            return new Pulse(x => amplitude * Math.Exp( - Math.Pow(x/sigma, 2)),3 * sigma);
        }

        public static Pulse RestoreSampledPulse(IEnumerable<PulsePoint> points, double period, double delta)
        {
            var lowerIndex = - points.Count()/2;
            Func<double, double> func = x => points.Select((p, i) => p.Y*Sinc(Math.PI/delta*(x - (i +lowerIndex)*delta))).Sum();
            return new Pulse(func, period);
        }

        public Pulse(Func<double, double> pulseFunc, double period)
        {
            _pulseFunc = pulseFunc;
            _period = period;
        }

        protected double Period
        {
            get { return _period; }
        }

        public double Y(double x)
        {
            return _pulseFunc(x);
        }

        public IEnumerable<PulsePoint> Sample(double delta)
        {
            var current = -2 * Period;
            while (current <= 2 * Period)
            {
                yield return new PulsePoint(current, _pulseFunc(current));
                current += delta;
            }
        }
        public IEnumerable<PulsePoint> TwinsSample(double delta)
        {
            var current = -2 * Period;
            var twin = 0;
            while (current <= 2 * Period)
            {
                yield return new PulsePoint(current, _pulseFunc(current)*Math.Pow(-1, twin));
                current += delta;
                ++twin;
            }
        }
        public virtual IEnumerable<PulsePoint> DataPoints()
        {
            var delta = 4 * Period / N;
            return Sample(delta);
        }

        public virtual IEnumerable<PulsePoint> TwinDataPoints()
        {
            return Sample(4*Period/N).Select((p, i) => new PulsePoint(p.X, p.Y*Math.Pow(-1, i)));
        }
    }
}
