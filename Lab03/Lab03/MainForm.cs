﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using MathNet.Numerics.IntegralTransforms;

namespace Lab01
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void GaussBtn_Click(object sender,  EventArgs e)
        {
            double amp = Double.Parse(Atb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double sigma = Double.Parse(Sigmatb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double delta = Double.Parse(GaussDeltatb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double period = 3*sigma;
            var squarePulse = SquarePulse.CreateSquarePulse(period);
            ProcessPulse(Pulse.CreateGaussPulse(amp, sigma), squarePulse, GaussSquare, delta);
            ProcessPoints(GaussSquare.Series["Square"], squarePulse.DataPoints());
        }

        void ProcessPulse(Pulse pulse1, Pulse pulse2, Chart chart, double delta)
        {
            var samples = pulse1.Sample(delta).Select(p => new Complex(p.Y, 0)).ToList();
            samples.AddRange(Enumerable.Repeat(new Complex(0,0), samples.Count-1));
            var samplesArray = samples.ToArray();
            var secondSamplesSet = pulse2.Sample(delta).Select(p => new Complex(p.Y, 0)).ToList();
            secondSamplesSet.AddRange(Enumerable.Repeat(new Complex(0, 0), secondSamplesSet.Count - 1));
            //secondSamplesSet.Reverse();
            var secondSamplesArray = secondSamplesSet.ToArray();
            Fourier.Forward(secondSamplesArray);
            Fourier.Forward(samplesArray);
            for (int i = 0; i < samplesArray.Length; ++i)
                samplesArray[i] *= secondSamplesArray[i];
            Fourier.Inverse(samplesArray);
            var result = samplesArray.Select((c, i) => new PulsePoint(i, c.Magnitude)).ToList();

            ProcessPoints(chart.Series["Original"], pulse1.DataPoints());
            ProcessPoints(chart.Series["FFT"], result);
        }

        void ProcessPoints(Series series, IEnumerable<PulsePoint> points)
        {
            series.Points.Clear();
            foreach (var pulsePoint in points)
            {
                series.Points.AddXY(pulsePoint.X, pulsePoint.Y);
            }
        }

        private void Squarebtn_Click(object sender, EventArgs e)
        {
            double delta = Double.Parse(SquareDeltattb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double period = Double.Parse(SquareTtb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            var squarePulse = SquarePulse.CreateSquarePulse(period);
            ProcessPulse(squarePulse,SquarePulse.CreateSquarePulse(period),  SquareChart, delta);
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double amp = Double.Parse(GaussGaussAtb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double sigma = Double.Parse(GaussGaussSigma.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double delta = Double.Parse(GaussGaussDt.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double period = 3 * sigma;
            ProcessPulse(Pulse.CreateGaussPulse(amp, sigma), Pulse.CreateGaussPulse(amp, sigma), GaussGauss, delta);
        
        }
    }
}
