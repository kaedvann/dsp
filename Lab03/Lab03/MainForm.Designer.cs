﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab01
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Squarebtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SquareDeltattb = new System.Windows.Forms.TextBox();
            this.SquareTlbl = new System.Windows.Forms.Label();
            this.SquareTtb = new System.Windows.Forms.TextBox();
            this.SquareChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.Sigmatb = new System.Windows.Forms.TextBox();
            this.GaussBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.GaussDeltatb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Atb = new System.Windows.Forms.TextBox();
            this.GaussSquare = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.GaussGaussSigma = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.GaussGaussDt = new System.Windows.Forms.TextBox();
            this.GaussGaussA = new System.Windows.Forms.Label();
            this.GaussGaussAtb = new System.Windows.Forms.TextBox();
            this.GaussGauss = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SquareChart)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussSquare)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussGauss)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(6);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(2548, 1192);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Squarebtn);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.SquareDeltattb);
            this.tabPage1.Controls.Add(this.SquareTlbl);
            this.tabPage1.Controls.Add(this.SquareTtb);
            this.tabPage1.Controls.Add(this.SquareChart);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage1.Size = new System.Drawing.Size(2540, 1154);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Прямоугольный импульс";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Squarebtn
            // 
            this.Squarebtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Squarebtn.Location = new System.Drawing.Point(2248, 977);
            this.Squarebtn.Margin = new System.Windows.Forms.Padding(6);
            this.Squarebtn.Name = "Squarebtn";
            this.Squarebtn.Size = new System.Drawing.Size(200, 44);
            this.Squarebtn.TabIndex = 5;
            this.Squarebtn.Text = "Рассчитать";
            this.Squarebtn.UseVisualStyleBackColor = true;
            this.Squarebtn.Click += new System.EventHandler(this.Squarebtn_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2242, 863);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Δt";
            // 
            // SquareDeltattb
            // 
            this.SquareDeltattb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SquareDeltattb.Location = new System.Drawing.Point(2248, 894);
            this.SquareDeltattb.Margin = new System.Windows.Forms.Padding(6);
            this.SquareDeltattb.Name = "SquareDeltattb";
            this.SquareDeltattb.Size = new System.Drawing.Size(196, 31);
            this.SquareDeltattb.TabIndex = 3;
            this.SquareDeltattb.Text = "0.5";
            // 
            // SquareTlbl
            // 
            this.SquareTlbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SquareTlbl.AutoSize = true;
            this.SquareTlbl.Location = new System.Drawing.Point(2242, 763);
            this.SquareTlbl.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.SquareTlbl.Name = "SquareTlbl";
            this.SquareTlbl.Size = new System.Drawing.Size(25, 25);
            this.SquareTlbl.TabIndex = 2;
            this.SquareTlbl.Text = "T";
            // 
            // SquareTtb
            // 
            this.SquareTtb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SquareTtb.Location = new System.Drawing.Point(2248, 794);
            this.SquareTtb.Margin = new System.Windows.Forms.Padding(6);
            this.SquareTtb.Name = "SquareTtb";
            this.SquareTtb.Size = new System.Drawing.Size(196, 31);
            this.SquareTtb.TabIndex = 1;
            this.SquareTtb.Text = "10";
            // 
            // SquareChart
            // 
            chartArea1.Name = "Signal";
            chartArea2.Name = "FFT";
            this.SquareChart.ChartAreas.Add(chartArea1);
            this.SquareChart.ChartAreas.Add(chartArea2);
            this.SquareChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.SquareChart.Legends.Add(legend1);
            this.SquareChart.Location = new System.Drawing.Point(6, 6);
            this.SquareChart.Margin = new System.Windows.Forms.Padding(6);
            this.SquareChart.Name = "SquareChart";
            series1.ChartArea = "Signal";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series1.Color = System.Drawing.Color.Blue;
            series1.Legend = "Legend1";
            series1.Name = "Original";
            series2.ChartArea = "FFT";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Color = System.Drawing.Color.Red;
            series2.Legend = "Legend1";
            series2.Name = "FFT";
            this.SquareChart.Series.Add(series1);
            this.SquareChart.Series.Add(series2);
            this.SquareChart.Size = new System.Drawing.Size(2528, 1142);
            this.SquareChart.TabIndex = 0;
            this.SquareChart.Text = "chart1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.Sigmatb);
            this.tabPage2.Controls.Add(this.GaussBtn);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.GaussDeltatb);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.Atb);
            this.tabPage2.Controls.Add(this.GaussSquare);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(6);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(6);
            this.tabPage2.Size = new System.Drawing.Size(2540, 1154);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Прямоугольник+Гаусс";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2276, 850);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 25);
            this.label4.TabIndex = 13;
            this.label4.Text = "σ";
            // 
            // Sigmatb
            // 
            this.Sigmatb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Sigmatb.Location = new System.Drawing.Point(2282, 881);
            this.Sigmatb.Margin = new System.Windows.Forms.Padding(6);
            this.Sigmatb.Name = "Sigmatb";
            this.Sigmatb.Size = new System.Drawing.Size(196, 31);
            this.Sigmatb.TabIndex = 12;
            this.Sigmatb.Text = "3";
            // 
            // GaussBtn
            // 
            this.GaussBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GaussBtn.Location = new System.Drawing.Point(2282, 1058);
            this.GaussBtn.Margin = new System.Windows.Forms.Padding(6);
            this.GaussBtn.Name = "GaussBtn";
            this.GaussBtn.Size = new System.Drawing.Size(200, 44);
            this.GaussBtn.TabIndex = 11;
            this.GaussBtn.Text = "Рассчитать";
            this.GaussBtn.UseVisualStyleBackColor = true;
            this.GaussBtn.Click += new System.EventHandler(this.GaussBtn_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2276, 944);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 25);
            this.label2.TabIndex = 10;
            this.label2.Text = "Δt";
            // 
            // GaussDeltatb
            // 
            this.GaussDeltatb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GaussDeltatb.Location = new System.Drawing.Point(2282, 975);
            this.GaussDeltatb.Margin = new System.Windows.Forms.Padding(6);
            this.GaussDeltatb.Name = "GaussDeltatb";
            this.GaussDeltatb.Size = new System.Drawing.Size(196, 31);
            this.GaussDeltatb.TabIndex = 9;
            this.GaussDeltatb.Text = "0.5";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2276, 760);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "A";
            // 
            // Atb
            // 
            this.Atb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Atb.Location = new System.Drawing.Point(2282, 790);
            this.Atb.Margin = new System.Windows.Forms.Padding(6);
            this.Atb.Name = "Atb";
            this.Atb.Size = new System.Drawing.Size(196, 31);
            this.Atb.TabIndex = 7;
            this.Atb.Text = "10";
            // 
            // GaussSquare
            // 
            chartArea3.Name = "Signal";
            chartArea4.Name = "FFT";
            this.GaussSquare.ChartAreas.Add(chartArea3);
            this.GaussSquare.ChartAreas.Add(chartArea4);
            this.GaussSquare.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.GaussSquare.Legends.Add(legend2);
            this.GaussSquare.Location = new System.Drawing.Point(6, 6);
            this.GaussSquare.Margin = new System.Windows.Forms.Padding(6);
            this.GaussSquare.Name = "GaussSquare";
            series3.ChartArea = "Signal";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series3.Color = System.Drawing.Color.Blue;
            series3.Legend = "Legend1";
            series3.Name = "Original";
            series4.ChartArea = "FFT";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.Color = System.Drawing.Color.Red;
            series4.Legend = "Legend1";
            series4.Name = "FFT";
            series5.ChartArea = "Signal";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series5.Legend = "Legend1";
            series5.Name = "Square";
            this.GaussSquare.Series.Add(series3);
            this.GaussSquare.Series.Add(series4);
            this.GaussSquare.Series.Add(series5);
            this.GaussSquare.Size = new System.Drawing.Size(2528, 1142);
            this.GaussSquare.TabIndex = 14;
            this.GaussSquare.Text = "chart1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.GaussGaussSigma);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.GaussGaussDt);
            this.tabPage3.Controls.Add(this.GaussGaussA);
            this.tabPage3.Controls.Add(this.GaussGaussAtb);
            this.tabPage3.Controls.Add(this.GaussGauss);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(2540, 1154);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Гаусс+Гаусс";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2276, 850);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 25);
            this.label5.TabIndex = 21;
            this.label5.Text = "σ";
            // 
            // GaussGaussSigma
            // 
            this.GaussGaussSigma.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GaussGaussSigma.Location = new System.Drawing.Point(2282, 881);
            this.GaussGaussSigma.Margin = new System.Windows.Forms.Padding(6);
            this.GaussGaussSigma.Name = "GaussGaussSigma";
            this.GaussGaussSigma.Size = new System.Drawing.Size(196, 31);
            this.GaussGaussSigma.TabIndex = 20;
            this.GaussGaussSigma.Text = "3";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(2282, 1058);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 44);
            this.button1.TabIndex = 19;
            this.button1.Text = "Рассчитать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2276, 944);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 25);
            this.label6.TabIndex = 18;
            this.label6.Text = "Δt";
            // 
            // GaussGaussDt
            // 
            this.GaussGaussDt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GaussGaussDt.Location = new System.Drawing.Point(2282, 975);
            this.GaussGaussDt.Margin = new System.Windows.Forms.Padding(6);
            this.GaussGaussDt.Name = "GaussGaussDt";
            this.GaussGaussDt.Size = new System.Drawing.Size(196, 31);
            this.GaussGaussDt.TabIndex = 17;
            this.GaussGaussDt.Text = "0.5";
            // 
            // GaussGaussA
            // 
            this.GaussGaussA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GaussGaussA.AutoSize = true;
            this.GaussGaussA.Location = new System.Drawing.Point(2276, 760);
            this.GaussGaussA.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.GaussGaussA.Name = "GaussGaussA";
            this.GaussGaussA.Size = new System.Drawing.Size(26, 25);
            this.GaussGaussA.TabIndex = 16;
            this.GaussGaussA.Text = "A";
            // 
            // GaussGaussAtb
            // 
            this.GaussGaussAtb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GaussGaussAtb.Location = new System.Drawing.Point(2282, 790);
            this.GaussGaussAtb.Margin = new System.Windows.Forms.Padding(6);
            this.GaussGaussAtb.Name = "GaussGaussAtb";
            this.GaussGaussAtb.Size = new System.Drawing.Size(196, 31);
            this.GaussGaussAtb.TabIndex = 15;
            this.GaussGaussAtb.Text = "10";
            // 
            // GaussGauss
            // 
            chartArea5.Name = "Signal";
            chartArea6.Name = "FFT";
            this.GaussGauss.ChartAreas.Add(chartArea5);
            this.GaussGauss.ChartAreas.Add(chartArea6);
            this.GaussGauss.Dock = System.Windows.Forms.DockStyle.Fill;
            legend3.Name = "Legend1";
            this.GaussGauss.Legends.Add(legend3);
            this.GaussGauss.Location = new System.Drawing.Point(0, 0);
            this.GaussGauss.Margin = new System.Windows.Forms.Padding(6);
            this.GaussGauss.Name = "GaussGauss";
            series6.ChartArea = "Signal";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series6.Color = System.Drawing.Color.Blue;
            series6.Legend = "Legend1";
            series6.Name = "Original";
            series7.ChartArea = "FFT";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series7.Color = System.Drawing.Color.Red;
            series7.Legend = "Legend1";
            series7.Name = "FFT";
            this.GaussGauss.Series.Add(series6);
            this.GaussGauss.Series.Add(series7);
            this.GaussGauss.Size = new System.Drawing.Size(2540, 1154);
            this.GaussGauss.TabIndex = 22;
            this.GaussGauss.Text = "chart1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2548, 1192);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SquareChart)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussSquare)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussGauss)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private Chart SquareChart;
        private TabPage tabPage2;
        private Button Squarebtn;
        private Label label1;
        private TextBox SquareDeltattb;
        private Label SquareTlbl;
        private TextBox SquareTtb;
        private Label label4;
        private TextBox Sigmatb;
        private Button GaussBtn;
        private Label label2;
        private TextBox GaussDeltatb;
        private Label label3;
        private TextBox Atb;
        private Chart GaussSquare;
        private TabPage tabPage3;
        private Label label5;
        private TextBox GaussGaussSigma;
        private Button button1;
        private Label label6;
        private TextBox GaussGaussDt;
        private Label GaussGaussA;
        private TextBox GaussGaussAtb;
        private Chart GaussGauss;
    }
}

