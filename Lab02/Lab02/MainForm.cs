﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using MathNet.Numerics.IntegralTransforms;

namespace Lab01
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void GaussBtn_Click(object sender, System.EventArgs e)
        {
            double amp = Double.Parse(Atb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double sigma = Double.Parse(Sigmatb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double delta = Double.Parse(GaussDeltatb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double period = 3*sigma;
            ProcessPulse(Pulse.CreateGaussPulse(amp, sigma),GaussChart, period, delta);
        }

        void ProcessPulse(Pulse pulse, Chart chart, double period, double delta)
        {
            var samples = pulse.Sample(delta).Select(p => new Complex(p.Y, 0)).ToArray();
            var dftSamples = samples.ToArray();
            Fourier.Forward(samples);
            dftSamples = Fourier.NaiveForward(dftSamples, FourierOptions.Default);
            var result = samples.Select((c, i) => new PulsePoint(i, c.Magnitude)).ToList();
            var dftResult = dftSamples.Select((c, i) => new PulsePoint(i, c.Magnitude)).ToList();
     

            ProcessPoints(chart.Series["Original"], pulse.DataPoints());
            ProcessPoints(chart.Series["FFT"], result);
            ProcessPoints(chart.Series["DFT"], dftResult);
            var twinSamples = pulse.TwinsSample(delta).Select(p => new Complex(p.Y, 0)).ToArray();
            var twinDftSamples = twinSamples.ToArray();
            Fourier.Forward(twinSamples);
            twinDftSamples = Fourier.NaiveForward(twinDftSamples, FourierOptions.Default);
            var twinResult = twinSamples.Select((c, i) => new PulsePoint(i, c.Magnitude)).ToList();
            var twinDftResult = twinDftSamples.Select((c, i) => new PulsePoint(i, c.Magnitude)).ToList();
            
            ProcessPoints(chart.Series["SignalNoTwins"], pulse.TwinDataPoints());
            ProcessPoints(chart.Series["FFTNoTwins"], twinResult);
            ProcessPoints(chart.Series["DFTNoTwins"], twinDftResult);
        }

        void ProcessPoints(Series series, IEnumerable<PulsePoint> points)
        {
            series.Points.Clear();
            foreach (var pulsePoint in points)
            {
                series.Points.AddXY(pulsePoint.X, pulsePoint.Y);
            }
        }

        private void Squarebtn_Click(object sender, EventArgs e)
        {
            double delta = Double.Parse(SquareDeltattb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double period = Double.Parse(SquareTtb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            ProcessPulse(SquarePulse.CreateSquarePulse(period), SquareChart, period, delta);
        }
    }
}
