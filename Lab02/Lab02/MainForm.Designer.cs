﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab01
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Squarebtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SquareDeltattb = new System.Windows.Forms.TextBox();
            this.SquareTlbl = new System.Windows.Forms.Label();
            this.SquareTtb = new System.Windows.Forms.TextBox();
            this.SquareChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.Sigmatb = new System.Windows.Forms.TextBox();
            this.GaussBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.GaussDeltatb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Atb = new System.Windows.Forms.TextBox();
            this.GaussChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SquareChart)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussChart)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1336, 620);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Squarebtn);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.SquareDeltattb);
            this.tabPage1.Controls.Add(this.SquareTlbl);
            this.tabPage1.Controls.Add(this.SquareTtb);
            this.tabPage1.Controls.Add(this.SquareChart);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1328, 594);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Прямоугольный импульс";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Squarebtn
            // 
            this.Squarebtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Squarebtn.Location = new System.Drawing.Point(1186, 508);
            this.Squarebtn.Name = "Squarebtn";
            this.Squarebtn.Size = new System.Drawing.Size(100, 23);
            this.Squarebtn.TabIndex = 5;
            this.Squarebtn.Text = "Рассчитать";
            this.Squarebtn.UseVisualStyleBackColor = true;
            this.Squarebtn.Click += new System.EventHandler(this.Squarebtn_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1183, 449);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Δt";
            // 
            // SquareDeltattb
            // 
            this.SquareDeltattb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SquareDeltattb.Location = new System.Drawing.Point(1186, 465);
            this.SquareDeltattb.Name = "SquareDeltattb";
            this.SquareDeltattb.Size = new System.Drawing.Size(100, 20);
            this.SquareDeltattb.TabIndex = 3;
            this.SquareDeltattb.Text = "0.5";
            // 
            // SquareTlbl
            // 
            this.SquareTlbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SquareTlbl.AutoSize = true;
            this.SquareTlbl.Location = new System.Drawing.Point(1183, 397);
            this.SquareTlbl.Name = "SquareTlbl";
            this.SquareTlbl.Size = new System.Drawing.Size(14, 13);
            this.SquareTlbl.TabIndex = 2;
            this.SquareTlbl.Text = "T";
            // 
            // SquareTtb
            // 
            this.SquareTtb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SquareTtb.Location = new System.Drawing.Point(1186, 413);
            this.SquareTtb.Name = "SquareTtb";
            this.SquareTtb.Size = new System.Drawing.Size(100, 20);
            this.SquareTtb.TabIndex = 1;
            this.SquareTtb.Text = "10";
            // 
            // SquareChart
            // 
            chartArea1.Name = "Signal";
            chartArea2.Name = "FFT";
            chartArea3.Name = "DFT";
            chartArea4.Name = "SignalTwins";
            chartArea5.Name = "FFTTwins";
            chartArea6.Name = "DFTTwins";
            this.SquareChart.ChartAreas.Add(chartArea1);
            this.SquareChart.ChartAreas.Add(chartArea2);
            this.SquareChart.ChartAreas.Add(chartArea3);
            this.SquareChart.ChartAreas.Add(chartArea4);
            this.SquareChart.ChartAreas.Add(chartArea5);
            this.SquareChart.ChartAreas.Add(chartArea6);
            this.SquareChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.SquareChart.Legends.Add(legend1);
            this.SquareChart.Location = new System.Drawing.Point(3, 3);
            this.SquareChart.Name = "SquareChart";
            series1.ChartArea = "Signal";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series1.Color = System.Drawing.Color.Blue;
            series1.Legend = "Legend1";
            series1.Name = "Original";
            series2.ChartArea = "FFT";
            series2.Color = System.Drawing.Color.Red;
            series2.Legend = "Legend1";
            series2.Name = "FFT";
            series3.ChartArea = "DFT";
            series3.Legend = "Legend1";
            series3.Name = "DFT";
            series4.ChartArea = "SignalTwins";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "SignalNoTwins";
            series5.ChartArea = "FFTTwins";
            series5.Legend = "Legend1";
            series5.Name = "FFTNoTwins";
            series6.ChartArea = "DFTTwins";
            series6.Legend = "Legend1";
            series6.Name = "DFTNoTwins";
            this.SquareChart.Series.Add(series1);
            this.SquareChart.Series.Add(series2);
            this.SquareChart.Series.Add(series3);
            this.SquareChart.Series.Add(series4);
            this.SquareChart.Series.Add(series5);
            this.SquareChart.Series.Add(series6);
            this.SquareChart.Size = new System.Drawing.Size(1322, 588);
            this.SquareChart.TabIndex = 0;
            this.SquareChart.Text = "chart1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.Sigmatb);
            this.tabPage2.Controls.Add(this.GaussBtn);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.GaussDeltatb);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.Atb);
            this.tabPage2.Controls.Add(this.GaussChart);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1328, 594);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Сигнал Гаусса";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1200, 442);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "σ";
            // 
            // Sigmatb
            // 
            this.Sigmatb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Sigmatb.Location = new System.Drawing.Point(1203, 458);
            this.Sigmatb.Name = "Sigmatb";
            this.Sigmatb.Size = new System.Drawing.Size(100, 20);
            this.Sigmatb.TabIndex = 12;
            this.Sigmatb.Text = "3";
            // 
            // GaussBtn
            // 
            this.GaussBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GaussBtn.Location = new System.Drawing.Point(1203, 550);
            this.GaussBtn.Name = "GaussBtn";
            this.GaussBtn.Size = new System.Drawing.Size(100, 23);
            this.GaussBtn.TabIndex = 11;
            this.GaussBtn.Text = "Рассчитать";
            this.GaussBtn.UseVisualStyleBackColor = true;
            this.GaussBtn.Click += new System.EventHandler(this.GaussBtn_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1200, 491);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Δt";
            // 
            // GaussDeltatb
            // 
            this.GaussDeltatb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GaussDeltatb.Location = new System.Drawing.Point(1203, 507);
            this.GaussDeltatb.Name = "GaussDeltatb";
            this.GaussDeltatb.Size = new System.Drawing.Size(100, 20);
            this.GaussDeltatb.TabIndex = 9;
            this.GaussDeltatb.Text = "0.5";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1200, 395);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "A";
            // 
            // Atb
            // 
            this.Atb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Atb.Location = new System.Drawing.Point(1203, 411);
            this.Atb.Name = "Atb";
            this.Atb.Size = new System.Drawing.Size(100, 20);
            this.Atb.TabIndex = 7;
            this.Atb.Text = "10";
            // 
            // GaussChart
            // 
            chartArea7.Name = "Signal";
            chartArea8.Name = "FFT";
            chartArea9.Name = "DFT";
            chartArea10.Name = "SignalTwins";
            chartArea11.Name = "FFTTwins";
            chartArea12.Name = "DFTTwins";
            this.GaussChart.ChartAreas.Add(chartArea7);
            this.GaussChart.ChartAreas.Add(chartArea8);
            this.GaussChart.ChartAreas.Add(chartArea9);
            this.GaussChart.ChartAreas.Add(chartArea10);
            this.GaussChart.ChartAreas.Add(chartArea11);
            this.GaussChart.ChartAreas.Add(chartArea12);
            this.GaussChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.GaussChart.Legends.Add(legend2);
            this.GaussChart.Location = new System.Drawing.Point(3, 3);
            this.GaussChart.Name = "GaussChart";
            series7.ChartArea = "Signal";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Color = System.Drawing.Color.Blue;
            series7.Legend = "Legend1";
            series7.Name = "Original";
            series8.ChartArea = "FFT";
            series8.Color = System.Drawing.Color.Red;
            series8.Legend = "Legend1";
            series8.Name = "FFT";
            series9.ChartArea = "DFT";
            series9.Legend = "Legend1";
            series9.Name = "DFT";
            series10.ChartArea = "SignalTwins";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Legend = "Legend1";
            series10.Name = "SignalNoTwins";
            series11.ChartArea = "FFTTwins";
            series11.Legend = "Legend1";
            series11.Name = "FFTNoTwins";
            series12.ChartArea = "DFTTwins";
            series12.Legend = "Legend1";
            series12.Name = "DFTNoTwins";
            this.GaussChart.Series.Add(series7);
            this.GaussChart.Series.Add(series8);
            this.GaussChart.Series.Add(series9);
            this.GaussChart.Series.Add(series10);
            this.GaussChart.Series.Add(series11);
            this.GaussChart.Series.Add(series12);
            this.GaussChart.Size = new System.Drawing.Size(1322, 588);
            this.GaussChart.TabIndex = 14;
            this.GaussChart.Text = "chart1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1336, 620);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SquareChart)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private Chart SquareChart;
        private TabPage tabPage2;
        private Button Squarebtn;
        private Label label1;
        private TextBox SquareDeltattb;
        private Label SquareTlbl;
        private TextBox SquareTtb;
        private Label label4;
        private TextBox Sigmatb;
        private Button GaussBtn;
        private Label label2;
        private TextBox GaussDeltatb;
        private Label label3;
        private TextBox Atb;
        private Chart GaussChart;
    }
}

