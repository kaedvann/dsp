﻿using System;
using System.Collections.Generic;

namespace Lab06
{
    public class SquarePulse: Pulse
    {
        public static SquarePulse CreateSquarePulse(double period)
        {
            return new SquarePulse(CreateSquarePulseFunc(period), period);
        }

        private static Func<double, double> CreateSquarePulseFunc(double period)
        {
            return x =>
            {
                if ((x > -period) && (x < period))
                    return 1;
                if (x == period || x == -period)
                    return 0.5;
                return 0;
            };
        }

        private SquarePulse(Func<double, double> pulseFunc, double period) : base(pulseFunc, period)
        {
        }

        public override IEnumerable<PulsePoint> DataPoints()
        {
            return new List<PulsePoint> { new PulsePoint(-2 * Period, 0), new PulsePoint(-Period, 0), new PulsePoint(-Period, 1), new PulsePoint(Period, 1), new PulsePoint(Period, 0), new PulsePoint(2 *Period, 0) };
        }
    }
}
