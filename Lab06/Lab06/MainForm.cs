﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.IntegralTransforms;
using MathNet.Numerics.Statistics;

namespace Lab06
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        
        Random _rand = new Random();

        void ProcessPulse(Pulse pulse1, Pulse pulse2, Chart chart, double delta)
        {
            var samples = pulse1.Sample(delta).Select(p => new Complex(p.Y, 0)).ToList();
            samples.AddRange(Enumerable.Repeat(new Complex(0,0), samples.Count-1));
            var samplesArray = samples.ToArray();
            var secondSamplesSet = pulse2.Sample(delta).Select(p => new Complex(p.Y, 0)).ToList();
            secondSamplesSet.AddRange(Enumerable.Repeat(new Complex(0, 0), secondSamplesSet.Count - 1));
            //secondSamplesSet.Reverse();
            var secondSamplesArray = secondSamplesSet.ToArray();
            Fourier.Forward(secondSamplesArray);
            Fourier.Forward(samplesArray);
            for (int i = 0; i < samplesArray.Length; ++i)
                samplesArray[i] *= secondSamplesArray[i];
            Fourier.Inverse(samplesArray);
            var result = samplesArray.Select((c, i) => new PulsePoint(i, c.Magnitude)).ToList();

            ProcessPoints(chart.Series["Original"], pulse1.DataPoints());
            ProcessPoints(chart.Series["FFT"], result);
        }

        void ProcessPoints(Series series, IEnumerable<PulsePoint> points)
        {
            series.Points.Clear();
            foreach (var pulsePoint in points)
            {
                series.Points.AddXY(pulsePoint.X, pulsePoint.Y);
            }
        }

        private void Squarebtn_Click(object sender, EventArgs e)
        {
            const double a = 10;
            const double sigma = 1;
            const double dt = 0.1;
            var pulse = Pulse.CreateGaussPulse(a, sigma);
            var samples = pulse.Sample(dt).ToArray();
            var uniformError = UniformErrors(samples.Count());
            var normError = NormErrors(samples.Count());
            var uniformGauss = samples.Select((s, i) => new PulsePoint(i, s.Y + uniformError[i])).ToArray();
            var normGauss = samples.Select((s, i) => new PulsePoint(i, s.Y + normError[i])).ToArray();
            ProcessPoints(GaussChart.Series["SignalUniform"], uniformGauss);
            ProcessPoints(GaussChart.Series["SignalNorm"], normGauss);
            ProcessPoints(GaussChart.Series["SignalUniformFilter"], Wiener(uniformGauss.Select(p => p.Y).ToArray(), uniformError).Select((r,i) => new PulsePoint(i, r)));
            ProcessPoints(GaussChart.Series["SignalNormFilter"], Wiener(normGauss.Select(p => p.Y).ToArray(), normError).Select((r, i) => new PulsePoint(i, r)));
        }
        private void button1_Click(object sender, EventArgs e)
        {
            const double a = 10;
            const double sigma = 3;
            const double dt = 0.1;
            var pulse = Pulse.CreateGaussPulse(a, sigma);
            var samples = pulse.Sample(dt).ToArray();
            var errors = SpikeErrors(samples.Length, 20, samples.Max(p => p.Y));
            var erroredSamples = samples.Select((p, i) => p.Y + errors[i]).ToArray();
            ProcessPoints(chart1.Series["Signal"], erroredSamples.Select((s,i)=>new PulsePoint(i,s)));
            ProcessPoints(chart1.Series["SignalMean"], FilterSpikes(erroredSamples, 0.1, Statistics.Mean).Select((r, i) => new PulsePoint(i, r)));
            ProcessPoints(chart1.Series["SignalMedian"], FilterSpikes(erroredSamples, 0.1, Statistics.Median).Select((r, i) => new PulsePoint(i, r)));
        }

        private IEnumerable<double> Near(double[] points, int index, int count)
        {
            int start = index - count/2;
            if (start < 0)
                start = 0;
            if (start + count >= points.Length)
                start = points.Length - count;
            var result = new double[count];
            for (int i = 0; i < count; ++i)
                result[i] = points[start + i];
            return result;
        }

        private double[] FilterSpikes(double[] signal, double tolerance, Func<IEnumerable<double>, double> smooth)
        {
            var result = new double[signal.Length];
            for (int i = 1; i < signal.Length - 1; ++i)
            {
                var corrected = smooth(Near(signal, i, 10));
                result[i] = Math.Abs(corrected - signal[i]) < tolerance ? signal[i] : corrected;
            }
            return result;
        }

        private double[] SpikeErrors(int count, int spikeCount, double maxSpike)
        {
            var errors = new double[spikeCount];
            var result = new double[count];
            for (int i = 0; i < errors.Length; ++i)
            {
                errors[i] = _rand.NextDouble()*maxSpike;
                result[_rand.Next(count - 1)] = errors[i];
            }
            return result;
        }

        private double[] NormErrors(int count)
        {
            var result = new double[count];
            Normal distrib = new Normal(5, 1);
            distrib.Samples(result);
            return result;
        }

        private double[] UniformErrors(int count)
        {
            var result = new double[count];
            for (int i = 0; i < result.Length; ++i)
            {
                result[i] = -1 + 2*_rand.NextDouble();
            }
            return result;
        }

        private Complex[] WienerCorrection(Complex[] signalSpectre, Complex[] noiseSpectre)
        {
            var result = new Complex[signalSpectre.Length];
            for (int i = 0; i < result.Length; ++i)
                result[i] = 1 - Complex.Pow(noiseSpectre[i]/signalSpectre[i], 2);
            return result;
        }

        private double[] Wiener(double[] signal, double[] noise)
        {
            var signalSpectre = signal.Select(s => new Complex(s, 0)).ToArray();
            var noiseSpectre = noise.Select(n => new Complex(n, 0)).ToArray();
            Fourier.Forward(signalSpectre);
            Fourier.Forward(noiseSpectre);
            var result = WienerCorrection(signalSpectre, noiseSpectre);
            for (int i = 0; i < result.Length; ++i)
                result[i] *= signalSpectre[i];
            Fourier.Inverse(result);
            return result.Select(r => r.Real).ToArray();
        }


    }
}
