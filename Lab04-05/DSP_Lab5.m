function DSP_Lab5
	gauss_mean = 0;
    gauss_sigma = 3;            % �����
    gauss_T = 3*gauss_sigma;    % ������
    gauss_dt = 0.1;             % ��� �������������
    gauss_A = 20;                % ���������
    gauss_width = gauss_T*1.5;
    gauss_plot = gauss(gauss_A, gauss_sigma, gauss_mean, gauss_T,...
        gauss_width, gauss_dt);
    
    %������
    R1 = normrnd(0,1,1,length(gauss_plot));
    R2 = rand([1 length(gauss_plot)]);
    dist1 = gauss_plot+R1;
    dist2 = gauss_plot+R2;
    [b, a] = butter(2, 0.1, 'high');
    out = filter(b, a, dist1);
    n = 10;
m = 2;
Wn = 0.2;

t  = fdesign.highpass('N,F3dB', 50, 0.7);
rr=design(t,'maxflat');

%[d,c] = maxflat(n,m,Wn,'high');
figure(1);
	subplot(2, 2, 1);
        plot(1:length(gauss_plot), dist1, 'r',...
        1:length(out), out, 'b');
        grid on; 
        title('Gauss with gaussian distortion Butterworth IIR');
	out = filter(b, a, dist2);
	subplot(2, 2, 2);
        plot(1:length(gauss_plot), dist2, 'r',...
        1:length(out), out, 'b');
        grid on; 
        title('Gauss with uniform distortion Butterworth IIR');
	subplot(2, 2, 3);
        plot(1:length(gauss_plot), dist1, 'r',...
        1:length(dist1), filter(rr, dist1), 'b');
        grid on; 
        title('Gauss with gaussian distortion Butterworth FIR');    
        
     	subplot(2, 2, 4);
        plot(1:length(gauss_plot), dist2, 'r',...
        1:length(out), filter(rr, dist2), 'b');
        grid on; 
        title('Gauss with uniform distortion Butterworth FIR');    
f = [0 0.48 0.48 1];
mhi = [0 0 1 1];
bhi = fir2(34,f,mhi);    
        
	bt = 0.3;
    span = 4;
    sps = 8;
    h = gaussdesign(bt,span,sps);
    y = filter(bhi,1,dist1);
    y2 = filter(bhi,1,dist2);
    figure(2);
    	subplot(1, 2, 1);
        plot(1:length(gauss_plot), dist1, 'r',...
        1:length(y), y, 'b');
        grid on; 
        title('Gauss with gaussian distortion FIR');    
     	subplot(1, 2, 2);
        plot(1:length(gauss_plot), dist2, 'r',...
        1:length(y2), y2, 'b');
        grid on; 
        title('Gauss with uniform distortion FIR');    
end



function [g] = gauss(A, sigma, mean, T, width, dt)
    points_num = ((mean + width) - (mean - width))/dt+1;
    g = zeros(1, points_num);
    for i=1:points_num;
        g(i) = A * exp(- (((i-1)*dt + (mean-width)) ^ 2) / sigma^2);
    end
end