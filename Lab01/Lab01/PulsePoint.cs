﻿namespace Lab01
{
    public struct PulsePoint
    {
        private readonly double _x;
        private readonly double _y;

        public PulsePoint(double x, double y)
        {
            _x = x;
            _y = y;
        }

        public double X { get { return _x; } }

        public double Y { get { return _y; } }
    }
}