﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab01
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.SquareChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.SquareTtb = new System.Windows.Forms.TextBox();
            this.SquareTlbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SquareDeltattb = new System.Windows.Forms.TextBox();
            this.Squarebtn = new System.Windows.Forms.Button();
            this.GaussBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.GaussDeltatb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Atb = new System.Windows.Forms.TextBox();
            this.GaussChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label4 = new System.Windows.Forms.Label();
            this.Sigmatb = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SquareChart)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussChart)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1336, 620);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Squarebtn);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.SquareDeltattb);
            this.tabPage1.Controls.Add(this.SquareTlbl);
            this.tabPage1.Controls.Add(this.SquareTtb);
            this.tabPage1.Controls.Add(this.SquareChart);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1328, 594);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Прямоугольный импульс";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // SquareChart
            // 
            chartArea1.Name = "ChartArea1";
            this.SquareChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.SquareChart.Legends.Add(legend1);
            this.SquareChart.Location = new System.Drawing.Point(8, 6);
            this.SquareChart.Name = "SquareChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StepLine;
            series1.Color = System.Drawing.Color.Blue;
            series1.Legend = "Legend1";
            series1.Name = "Original";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Color = System.Drawing.Color.Red;
            series2.Legend = "Legend1";
            series2.Name = "Restored";
            this.SquareChart.Series.Add(series1);
            this.SquareChart.Series.Add(series2);
            this.SquareChart.Size = new System.Drawing.Size(1053, 580);
            this.SquareChart.TabIndex = 0;
            this.SquareChart.Text = "chart1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.Sigmatb);
            this.tabPage2.Controls.Add(this.GaussBtn);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.GaussDeltatb);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.Atb);
            this.tabPage2.Controls.Add(this.GaussChart);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1328, 594);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Сигнал Гаусса";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // SquareTtb
            // 
            this.SquareTtb.Location = new System.Drawing.Point(1127, 61);
            this.SquareTtb.Name = "SquareTtb";
            this.SquareTtb.Size = new System.Drawing.Size(100, 20);
            this.SquareTtb.TabIndex = 1;
            this.SquareTtb.Text = "10";
            // 
            // SquareTlbl
            // 
            this.SquareTlbl.AutoSize = true;
            this.SquareTlbl.Location = new System.Drawing.Point(1124, 45);
            this.SquareTlbl.Name = "SquareTlbl";
            this.SquareTlbl.Size = new System.Drawing.Size(14, 13);
            this.SquareTlbl.TabIndex = 2;
            this.SquareTlbl.Text = "T";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1124, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Δt";
            // 
            // SquareDeltattb
            // 
            this.SquareDeltattb.Location = new System.Drawing.Point(1127, 113);
            this.SquareDeltattb.Name = "SquareDeltattb";
            this.SquareDeltattb.Size = new System.Drawing.Size(100, 20);
            this.SquareDeltattb.TabIndex = 3;
            this.SquareDeltattb.Text = "0.1";
            // 
            // Squarebtn
            // 
            this.Squarebtn.Location = new System.Drawing.Point(1127, 156);
            this.Squarebtn.Name = "Squarebtn";
            this.Squarebtn.Size = new System.Drawing.Size(100, 23);
            this.Squarebtn.TabIndex = 5;
            this.Squarebtn.Text = "Рассчитать";
            this.Squarebtn.UseVisualStyleBackColor = true;
            this.Squarebtn.Click += new System.EventHandler(this.Squarebtn_Click);
            // 
            // GaussBtn
            // 
            this.GaussBtn.Location = new System.Drawing.Point(1125, 200);
            this.GaussBtn.Name = "GaussBtn";
            this.GaussBtn.Size = new System.Drawing.Size(100, 23);
            this.GaussBtn.TabIndex = 11;
            this.GaussBtn.Text = "Рассчитать";
            this.GaussBtn.UseVisualStyleBackColor = true;
            this.GaussBtn.Click += new System.EventHandler(this.GaussBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1122, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Δt";
            // 
            // GaussDeltatb
            // 
            this.GaussDeltatb.Location = new System.Drawing.Point(1125, 157);
            this.GaussDeltatb.Name = "GaussDeltatb";
            this.GaussDeltatb.Size = new System.Drawing.Size(100, 20);
            this.GaussDeltatb.TabIndex = 9;
            this.GaussDeltatb.Text = "0.01";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1122, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "A";
            // 
            // Atb
            // 
            this.Atb.Location = new System.Drawing.Point(1125, 61);
            this.Atb.Name = "Atb";
            this.Atb.Size = new System.Drawing.Size(100, 20);
            this.Atb.TabIndex = 7;
            this.Atb.Text = "1";
            // 
            // GaussChart
            // 
            chartArea2.Name = "ChartArea1";
            this.GaussChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.GaussChart.Legends.Add(legend2);
            this.GaussChart.Location = new System.Drawing.Point(6, 6);
            this.GaussChart.Name = "GaussChart";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Legend = "Legend1";
            series3.Name = "Original";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.Color = System.Drawing.Color.Red;
            series4.Legend = "Legend1";
            series4.Name = "Restored";
            this.GaussChart.Series.Add(series3);
            this.GaussChart.Series.Add(series4);
            this.GaussChart.Size = new System.Drawing.Size(1053, 580);
            this.GaussChart.TabIndex = 6;
            this.GaussChart.Text = "chart2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1122, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "σ";
            // 
            // Sigmatb
            // 
            this.Sigmatb.Location = new System.Drawing.Point(1125, 108);
            this.Sigmatb.Name = "Sigmatb";
            this.Sigmatb.Size = new System.Drawing.Size(100, 20);
            this.Sigmatb.TabIndex = 12;
            this.Sigmatb.Text = "0.5";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1336, 620);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SquareChart)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GaussChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private Chart SquareChart;
        private TabPage tabPage2;
        private Button Squarebtn;
        private Label label1;
        private TextBox SquareDeltattb;
        private Label SquareTlbl;
        private TextBox SquareTtb;
        private Label label4;
        private TextBox Sigmatb;
        private Button GaussBtn;
        private Label label2;
        private TextBox GaussDeltatb;
        private Label label3;
        private TextBox Atb;
        private Chart GaussChart;
    }
}

