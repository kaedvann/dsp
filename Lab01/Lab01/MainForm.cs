﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab01
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void GaussBtn_Click(object sender, System.EventArgs e)
        {
            double amp = Double.Parse(Atb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double sigma = Double.Parse(Sigmatb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double delta = Double.Parse(GaussDeltatb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double period = 3*sigma;
            ProcessPulse(Pulse.CreateGaussPulse(amp, sigma),GaussChart, period, delta);
        }

        void ProcessPulse(Pulse pulse, Chart chart, double period, double delta)
        {
            var samples = pulse.Sample(delta).ToList();
            var restoredPulse = Pulse.RestoreSampledPulse(samples, period, delta);
            ProcessPoints(chart.Series["Original"], pulse.DataPoints());
            ProcessPoints(chart.Series["Restored"], restoredPulse.DataPoints());
        }

        void ProcessPoints(Series series, IEnumerable<PulsePoint> points)
        {
            series.Points.Clear();
            foreach (var pulsePoint in points)
            {
                series.Points.AddXY(pulsePoint.X, pulsePoint.Y);
            }
        }

        private void Squarebtn_Click(object sender, EventArgs e)
        {
            double delta = Double.Parse(SquareDeltattb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            double period = Double.Parse(SquareTtb.Text.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture);
            ProcessPulse(SquarePulse.CreateSquarePulse(period), SquareChart, period, delta);
        }
    }
}
